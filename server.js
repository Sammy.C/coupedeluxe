const express = require("express")
const bodyParser = require("body-parser")
const {sequelize} = require("./models")
const  cors  = require('cors')
const passport = require('passport')
const morgan = require('morgan')
const helmet = require('helmet')
const session = require('express-session')



const server = express()
server.use(cors())
server.use(morgan('combined'))
server.use(helmet())
//sessions
server.use(
    session({
    secret: "Pfe'R_]CvHl}UK-", 
    resave: false,
    saveUninitialized: false 
    })
)
server.use(bodyParser.urlencoded({ extended: false }))
server.use(bodyParser.json())
//Initiliaze Passport
server.use(passport.initialize())
require("./router")(server)
sequelize.sync().then(
    () => {
        server.listen(3005, () => {
            console.log('Server listing on port 3005');
        });
    }
)
