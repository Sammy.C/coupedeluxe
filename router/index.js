const AuthController = require('../controllers/AuthUser');
const BookingController = require('../controllers/Booking')
const passPortObj = require('../config/passport-setup')
const passport = require('passport')
module.exports = (server) => {
    server.post('/api/register', AuthController.register)
    //server.post('/auth/google',passport.authenticate('googleStrategy' , {session:false}), AuthController.google)
    server.post('/auth/facebook',passport.authenticate('facebookStrategy', {session: false}), AuthController.facebook)
     //server.post('/appointment', passport.authenticate('jwt', {session:false}), BookingController.create)
}