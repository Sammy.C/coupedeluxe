//External Imports
const passport = require("passport");
const JWTStrategy = require("passport-jwt").Strategy;
const { ExtractJwt } = require("passport-jwt");
const GoogleStrategy = require("passport-google-plus-token");
const FacebookStrategy = require("passport-facebook-token");
//App imports
const config = require("./configObj");
const {Clients} = require("../models");
//JWT
passport.use(
  new JWTStrategy(
    {
      jwtFromRequest: ExtractJwt.fromHeader("authorization"),
      secretOrKey: config.auth.jwt.val
    },
    async (payload, done) => {
      try {
        console.log("JWT STRATEGY HAS BEEN TRIGGERED");
        console.log(payload);
        const user = await Clients.find({
          where: {
            email: payload.sub[1]
          }
        });
        if (!user) return done(null, false);

        done(null, user);
      } catch (error) {
        done(error, false);
      }
    }
  )
);

passport.use(
  "facebookStrategy",
  new FacebookStrategy(
    {
      clientID: config.auth.fb.id,
      clientSecret: config.auth.fb.secret
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        console.log("I AM HERER")
        console.log(accessToken);
        console.log(refreshToken);
        console.log(profile);
        const email = profile.emails[0].value;
        const firstName = profile.name.givenName;
        const lastName = profile.name.familyName;
        const existingUser = await Clients.findOne({
          where: {
            email: email
          }
        });

        if (existingUser) {
          console.log(
            "This User Already exist in our DB / using FACEBOOK LOGIN STRATEGY " +
              email
          );
          return done(null, existingUser);
        }

        //iF HITS THIS LINE MEANS NEW USER
        const newUser = await Clients.create({
          email: email,
          firstName: firstName,
          lastName: lastName
        });
        done(null, newUser);
      } catch (error) {
        done(error, false, error.message);
      }
    }
  )
);

//Google Strategy
passport.use(
  "googleStrategy",
  new GoogleStrategy(
    {
      clientID: "dfhkdfggdfjhkgfd",
      clientSecret: "gjklfdjgdfjklgfdkjl"
    },
    async (accessToken, refreshToken, profile, done) => {
      try {
        console.log(accessToken);
        console.log(refreshToken);
        console.log(profile);
        const email = profile.emails[0].value;
        const firstName = profile.name.familyName;
        const lastName = profile.name.givenName;
        const existingUser = await Clients.findOne({
          where: {
            email: email
          }
        });

        if (existingUser) {
          console.log(
            "This User Already exist in our DB / using GOOGLE LOGIN STRATEGY " +
              email
          );
          return done(null, existingUser);
        }

        //iF HITS THIS LINE MEANS NEW USER
        const newUser = await Clients.create({
          email: email,
          firstName: firstName,
          lastName: lastName
        });
        done(null, newUser);
      } catch (error) {
        done(error, false, error.message);
      }
    }
  )
);
