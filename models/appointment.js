module.exports = (sequelize, DataTypes) => {
  const Appointment = sequelize.define('Appointment', {
    appt: {
      type:DataTypes.DATE
    },
    startTime:{
      type:DataTypes.DATE
    },
    endTime:{
      type:DataTypes.DATE
    },
    date: {
      type: DataTypes.DATE
    }
  });
  Appointment.associate = function(models) {
    Appointment.belongsTo(models.Clients)
    Appointment.belongsTo(models.Barbers)
  };
  return Appointment;
};