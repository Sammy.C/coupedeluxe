const bcrypt  = require('bcrypt')

function hashPassword (user, options) {
  const SALT_FACTOR = 8

  if (!user.changed('password')) {
    return
  }

  return bcrypt
    .genSaltAsync(SALT_FACTOR)
    .then(salt => bcrypt.hashAsync(user.password, salt, null))
    .then(hash => {
      user.setDataValue('password', hash)
    })
}
module.exports = (sequelize, DataTypes) => {
  const Barbers = sequelize.define('Barbers', {
    firstName: {
      type:DataTypes.STRING
    },
    lastName: {
      type: DataTypes.STRING
    },
    phoneNumber:{
      type: DataTypes.STRING
    },
    confirmed:{
      type: DataTypes.BOOLEAN
    },
    email:{
      type: DataTypes.STRING
    },
    intervalJson:{
      type: DataTypes.JSON
    },
    startTime:{
      type: DataTypes.DATE
    },
    endTime:{
      type: DataTypes.DATE
    },
}, {
  hooks: {
    beforeCreate: hashPassword,
    beforeUpdate: hashPassword,
    beforeSave: hashPassword
  }
});
Barbers.prototype.comparePassword = function (password) {
  return bcrypt.compareAsync(password, this.password)
}
  Barbers.associate = function(models) {
    Barbers.hasMany(models.Appointment)
    
  };
  return Barbers;
};