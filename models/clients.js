const bcrypt  = require('bcrypt')

function hashPassword (user, options) {
  const SALT_FACTOR = 8

  if (!user.changed('password')) {
    return
  }
  console.log("This is the plain password" , user.password);
  return bcrypt
    .genSalt(SALT_FACTOR)
    .then(salt => {return bcrypt.hash(user.password, salt)})
    .then(hash => {
      user.setDataValue('password', hash)
    })
}
module.exports = (sequelize, DataTypes) => {
  const Clients = sequelize.define('Clients', {
    firstName: {
      type:DataTypes.STRING
    },
    lastName: {
      type:DataTypes.STRING
    },
    age:{
      type:DataTypes.INTEGER
    },
    city:{
      type: DataTypes.STRING
    },
    email:{
      type: DataTypes.STRING,
      unique:true
    },
    password:{
      type: DataTypes.STRING,
    },
  }, {
    hooks: {
      beforeCreate: hashPassword,
      beforeUpdate: hashPassword,
      beforeSave: hashPassword
    }
  });
  Clients.prototype.comparePassword = function (password) {
    return bcrypt.compareAsync(password, this.password)
  }

  Clients.associate = function(models) {
  };
  return Clients;
};