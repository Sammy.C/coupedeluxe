module.exports = (sequelize, DataTypes) => {
  const Services = sequelize.define('Services', {
    name: {
      type:DataTypes.STRING
    },
    price:{
      type: DataTypes.DOUBLE
    },
    time:{
      type: DataTypes.DATE
    }

  });
  Services.associate = function(models) {
    // associations can be defined here
  };
  return Services;
};