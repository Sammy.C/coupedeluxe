const bcrypt  = require('bcrypt')

function hashPassword (user, options) {
  const SALT_FACTOR = 8

  if (!user.changed('password')) {
    return
  }

  return bcrypt
    .genSaltAsync(SALT_FACTOR)
    .then(salt => bcrypt.hashAsync(user.password, salt, null))
    .then(hash => {
      user.setDataValue('password', hash)
    })
}


module.exports = (sequelize, DataTypes) => {
  const Managers = sequelize.define('Managers', {
    firstName: {
      type:DataTypes.STRING
    },
    lastName: {
      type:DataTypes.STRING
    },
    password: {
      type:DataTypes.STRING
    },
    email:{
      type:DataTypes.STRING
    },
    firstName: {
      type:DataTypes.STRING
    },
  },{
    hooks: {
      beforeCreate: hashPassword,
      beforeUpdate: hashPassword,
      beforeSave: hashPassword
    }
  });
  Managers.prototype.comparePassword = function (password) {
    return bcrypt.compareAsync(password, this.password)
  }
  Managers.associate = function(models) {
    // associations can be defined here
  };
  return Managers;
};