'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Clients', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      firstName: {
        type:Sequelize.STRING
      },
      lastName: {
        type:Sequelize.STRING
      },
      age:{
        type:Sequelize.INTEGER
      },
      city:{
        type: Sequelize.STRING
      },
      email:{
        type: Sequelize.STRING,
        unique:true
      },
      password:{
        type: Sequelize.STRING,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Clients');
  }
};