const {Managers} = require('../models')
module.exports = {
    async register (req, res) {
        try{
            console.log(req.body);
            const managers = await Managers.create(req.body)
            console.log("User has been created");
            const managersJson = managers.toJSON()
            res.send({
              user: managersJson,
              token: "HELLOWORLD"//implement JWT token in the future
            })
        }catch(err){
            res.status(400).send({
                error: err.message
            })
        }
    }
}