const JWT = require("jsonwebtoken");
const { Clients } = require("../models");
const config = require("../config/configObj");

signToken = user => {
  return JWT.sign(
    {
      iss: "CoupeDeLuxe",
      sub: [user.email, user.id],
      iat: new Date().getTime(),
      exp: new Date().setDate(new Date().getDate() + 1) //current time + 1 day
    },
    config.auth.jwt.val
  );
};

module.exports = {
  register: async (req, res, next) => {
    try {
      console.log(req.body);
      const user = await Clients.create(req.body);
      console.log("User has been created");
      const userJson = user.toJSON();
      const token = signToken(user);
      res.send({
        user: userJson,
        token: token
      });
    } catch (err) {
      res.status(400).send({
        error: err.message
      });
    }
  },
  google: async (req, res,next) => {
    // Should print the information of the user that has tried to register/login through our app 
    console.log("GOOGLE AUTHENTICATION")
    console.log(req.user)
    const token = signToken(req.user)
    res.status(200).send({
        user: req.user.toJSON(),
        token: token
    });
  },
  facebook: async(req,res,next) =>{
    // Should print the information of the user that has tried to register/login through our app 
    console.log("FACEBOOK AUTHENTICATION")
    console.log(req.user)
    const token = signToken(req.user)
    res.status(200).send({
        user: req.user.toJSON(),
        token: token
    });
  },
  logout: async (req, res) => {
    req.session.destroy(function(err) {
      res.send({
        userLogout: true
      });
    });
  }
};
